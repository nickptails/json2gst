#include "stdlib.h"
#include "stdio.h"
#include "glib.h"
#include "json-glib/json-glib.h"
#include "gst/gst.h"
#include "gst/pbutils/pbutils.h"
#include "parser.h"


static gboolean preset = FALSE, profile = FALSE, owr = FALSE;
static gchar* path = NULL;

static GOptionEntry entries[] =
{
    { "preset", 's', 0, G_OPTION_ARG_NONE, &preset, "Input file contains a preset (alternative to profile)", NULL },
    { "profile", 'p', 0, G_OPTION_ARG_NONE, &profile, "Input file represents a profile (alternative to preset)", NULL },
    { "input", 'i', 0, G_OPTION_ARG_STRING, &path, "Input file", "PATH" },
    { "force", 'f', 0, G_OPTION_ARG_NONE, &owr, "Overwrite entity if one with the same name exists", NULL },
    { NULL }
};

void display_help(GOptionContext* context) {
    gchar* helpstr = g_option_context_get_help(context, TRUE, NULL);
    fprintf(stderr, "%s", helpstr);
    g_free(helpstr);
}

gboolean parse_args (int argc, char** argv) {
    GOptionContext *context;
    GError* error;
    context = g_option_context_new ("Insert GStreamer resource from JSON");
    g_option_context_add_main_entries (context, entries, NULL);
    g_option_context_add_group (context, gst_init_get_option_group());
    if (!g_option_context_parse (context, &argc, &argv, &error))
    {
        display_help(context);
        fprintf (stderr, "Failed option parsing: %s\n", error->message);
        g_error_free(error);
        return FALSE;
    }
    if (preset == profile) {
        display_help(context);
        fprintf(stderr, "Cannot set a preset and a profile at the same time..\n");
        return FALSE;
    }
    if (path == NULL) {
        display_help(context);
        fprintf(stderr, "No path given..\n");
        return FALSE;
    }
    return TRUE;
}

gboolean inject_preset () {
    JsonParser* parser = json_parser_new();
    JsonReader* reader;
    GstPreset* preset;
    char* pname;
    GError* err;
    if (!json_parser_load_from_file(parser, path, &err)) {
        fprintf(stderr, "Error while parsing JSON %s:\t%s\n", path, err -> message);
        g_error_free(err);
        g_object_unref(parser);
        return FALSE;
    }
    reader = json_reader_new(json_parser_get_root(parser));
    if (!json_reader_read_member(reader, JSON_GST_FIELD_PRESET)) {
        err = (GError*) json_reader_get_error(reader);
        if (err -> code != JSON_READER_ERROR_INVALID_MEMBER) {
            fprintf(stderr, "Error while reading preset from JSON %s:\t%s\n", path, err -> message);
            json_reader_end_member(reader);
            g_object_unref(reader);
            g_object_unref(parser);
            return FALSE;
        }
        json_reader_end_member(reader);
        g_object_unref(reader);
        g_object_unref(parser);
        return TRUE;
    }
    preset = json_gst_preset_load(reader, &pname);
    json_reader_end_member(reader);
    if (preset == NULL) {
        fprintf(stderr, "Unable to load profile from JSON file..\n");
        g_object_unref(reader);
        g_object_unref(parser);
        return FALSE;
    }
    if (!owr && json_gst_preset_exists(pname, GST_OBJECT_NAME(gst_element_get_factory(GST_ELEMENT(preset))))) {
        fprintf(stderr, "Profile with the given name already exists..\n");
        gst_object_unref(preset);
        free(pname);
        g_object_unref(reader);
        g_object_unref(parser);
        return FALSE;
    }
    if (!gst_preset_save_preset(preset, pname)) {
        fprintf(stderr, "Error while saving preset into the system..\n");
        gst_object_unref(preset);
        free(pname);
        g_object_unref(reader);
        g_object_unref(parser);
        return FALSE;
    }
    gst_object_unref(preset);
    free(pname);
    g_object_unref(reader);
    g_object_unref(parser);
    return TRUE;
}

int main (int argc, char *argv[])
{
    if (!parse_args(argc, argv) || !inject_preset())
        return EXIT_FAILURE;
    return EXIT_SUCCESS;
}
