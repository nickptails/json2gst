#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "json-glib/json-glib.h"
#include "gst/gst.h"
#include "gst/pbutils/pbutils.h"
#include "parser.h"


char* json_reader_get_member_string(JsonReader* reader, const char* member) {
    char* mstr;
    GError* err;
    if (!json_reader_read_member(reader, member)) {
        err = (GError*) json_reader_get_error(reader);
        fprintf(stderr, "Error while reading member %s:\t%s\n", member, err -> message);
        json_reader_end_member(reader);
        return NULL;
    }
    mstr = (char*) json_reader_get_string_value(reader);
    if (mstr == NULL) {
        err = (GError*) json_reader_get_error(reader);
        fprintf(stderr, "Error while reading value of member %s:\t%s\n", member, err -> message);
        json_reader_end_member(reader);
        return NULL;
    }
    mstr = strdup(mstr);    /* copy string since the value is held by the parser */
    json_reader_end_member(reader);
    if (mstr == NULL) {
        fprintf(stderr, "Error while copying value of member %s\n", member);
        return NULL;
    }
    return mstr;
}

gboolean json_preset_read_properties (JsonReader* reader, GObject* obj) {
    gchar** mlist, **miter;
    GError* err;
    mlist = json_reader_list_members(reader);
    for (miter = mlist; *miter != NULL; miter++) {
        GValue value = G_VALUE_INIT;
        if (!json_reader_read_member(reader, *miter)) {
            err = (GError*) json_reader_get_error(reader);
            fprintf(stderr, "Error while reading property %s:\t%s\n", *miter, err -> message);
            json_reader_end_member(reader);
            g_strfreev(mlist);
            return FALSE;
        }
        if (!json_reader_is_value(reader)) {
            fprintf(stderr, "Complex type not supported for property %s\n", *miter);
            json_reader_end_member(reader);
            continue;
        }
        json_node_get_value(json_reader_get_value(reader), &value);
        g_object_set_property(obj, *miter, &value);
        g_value_unset(&value);
        json_reader_end_member(reader);
    }
    g_strfreev(mlist);
    return TRUE;
}

GstPreset* json_gst_preset_load (JsonReader* reader, char** name) {
    char* fname, *ename;
    GstElement* element;
    GError* err;
    if (name != NULL) {
        *name = json_reader_get_member_string(reader, JSON_GST_FIELD_PRESET_NAME);
        if (*name == NULL) {
            fprintf(stderr, "Unable to get member string from JSON..\n");
            return NULL;
        }
    }
    fname = json_reader_get_member_string(reader, JSON_GST_FIELD_ELEMENT_FACTORY_NAME);
    ename = json_reader_get_member_string(reader, JSON_GST_FIELD_ELEMENT_NAME);
    if (fname == NULL || ename == NULL ) {
        fprintf(stderr, "Unable to get member string from JSON..\n");
        free(fname);
        free(ename);
        if (name != NULL)
            free(*name);
        return NULL;
    }
    element = gst_element_factory_make(fname, ename);
    free(fname);
    free(ename);
    if (element == NULL) {
        fprintf(stderr, "Unable to create empty preset for element %s\n", fname);
        if (name != NULL)
            free(*name);
        return NULL;
    }
    if (!json_reader_read_member(reader, JSON_GST_FIELD_PROPERTY_LIST)) {
        err = (GError*) json_reader_get_error(reader);
        if (err -> code != JSON_READER_ERROR_INVALID_MEMBER) {      /* allow missing property list */
            fprintf(stderr, "Error while parsing preset properties:\t%s\n", err -> message);
            json_reader_end_member(reader);
            if (name != NULL)
                free(*name);
            return NULL;
        }
        json_reader_end_member(reader);
        return GST_PRESET(element);
    }
    if (!json_reader_is_object(reader)) {
        fprintf(stderr, "Malformed field for property list..\n");
        json_reader_end_member(reader);
        if (name != NULL)
            free(*name);
        return FALSE;
    }
    if (!json_preset_read_properties(reader, G_OBJECT(element))) {
        fprintf(stderr, "Unable to set properties on preset..\n");
        json_reader_end_member(reader);
        if (name != NULL)
            free(*name);
        return NULL;
    }
    json_reader_end_member(reader);
    return GST_PRESET(element);
}

gboolean json_gst_preset_exists (const char* name, const char* factory) {
    gboolean existing = FALSE;
    GstElement* element = gst_element_factory_make(factory, NULL);
    if (element == NULL) {
        fprintf(stderr, "Unable to create empty preset for %s..\n", factory);
        return FALSE;
    }
    existing = gst_preset_load_preset(GST_PRESET(element), name);
    gst_object_unref(element);
    return existing;
}

GstEncodingProfile* json_gst_profile_load (const char* jpath, char** name) {
    return NULL;
}

gboolean json_gst_profile_exists (const char* name) {
    return TRUE;
}
