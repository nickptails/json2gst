#include "gst/gst.h"
#include "gst/pbutils/pbutils.h"


#define JSON_GST_FIELD_PRESET "preset"
#define JSON_GST_FIELD_PRESET_NAME "name"
#define JSON_GST_FIELD_ELEMENT_FACTORY_NAME "element"
#define JSON_GST_FIELD_ELEMENT_NAME "element-name"
#define JSON_GST_FIELD_PROPERTY_LIST "properties"


/** Load Gstreamer preset from JSON.
 * @param reader JSON reader positioned on the preset object.
 * @param[out] name Preferred identifier for extracted preset.
 * @return Newly allocated preset.
 *
 * Read an object from JSON and allocate a new unnamed preset
 * according to properties specified in the given file. 
 */
GstPreset* json_gst_preset_load (JsonReader* reader, char** name);

/** Check preset existence.
 * @param name Preset identifier.
 * @param factory Element factory name for the preset.
 * @return Whether a profile with the given data is already present within the
 * GStreamer framework.
 */
gboolean json_gst_preset_exists (const char* name, const char* factory);

/** Load Gstreamer profile from JSON file.
 * @param jpath Path of JSON file.
 * @param[out] name Identifier of profile as specified from input.
 * @return Newly allocated profile.
 *
 * Read JSON file from the given path and allocate a new unnamed profile
 * according to properties specified in the given file. 
 */
GstEncodingProfile* json_gst_profile_load (const char* jpath, char** name);

/** Check profile existence.
 * @param name Identifier to verify.
 * @return Whether a profile with the given name is already present within the
 * GStreamer framework.
 */
gboolean json_gst_profile_exists (const char* name);
